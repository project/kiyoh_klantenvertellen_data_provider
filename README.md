Module to fetch, save and serve klantenvertellen results. This module is only responsible for downloading and caching the feed-content. Feed is downloaded and cached on every cron-run.

There is a service which you can use to use the data of the feed.

**Example**

```php
try {
  $klantenvertellen = \Drupal::service('klantenvertellen.feed')->getData();
  return ['#markup' => 'Aantal sterren: ' . $klantenvertellen->getStars()];
} catch (\Drupal\klantenvertellen\Exception\FeedNotCachedException $e) {
  return ['#markup' => $this->t('Data cannot be loaded. Is the feed cached?')];
}
```

See `\Drupal\klantenvertellen\Klantenvertellen::class` for complete API