<?php

namespace Drupal\klantenvertellen\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use stdClass;

/**
 * Class KlantenvertellenForm.
 */
class KlantenvertellenForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'klantenvertellen.feedparameters',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'klantenvertellen_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('klantenvertellen.feedparameters');
    $form['feed_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feed URL'),
      '#description' => $this->t('The URL of the XMLfeed'),
      '#maxlength' => 255,
      '#size' => 200,
      '#default_value' => $config->get('feed_url'),
    ];

    $form['statusinformation'] = [
      '#markup' => $this->t('
      <h2>
      Development information
      </h2>
      <p>
      This module is only responsible for downloading and caching the feed-content. Feed is downloaded on every cron-run
      </p>
      <p>
      There is a service which you can use to use the data of the feed.
      </p>
      <p>
      <strong>Example</strong>
      <code>
      <pre>
try {
  $klantenvertellen = \Drupal::service(\'klantenvertellen.feed\')->getData();
  return [\'#markup\' => \'Aantal sterren: \' . $klantenvertellen->getStars()];
} catch (\Drupal\klantenvertellen\Exception\FeedNotCachedException $e) {
  return [\'#markup\' => $this->t(\'Data cannot be loaded. Is the feed cached?\')];
}</pre>
      </code>
      See <code>\Drupal\klantenvertellen\Klantenvertellen::class</code> for complete API
      </p>')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $this->config('klantenvertellen.feedparameters')
      ->set('feed_url', $form_state->getValue('feed_url'))
      ->save();
  }
}
