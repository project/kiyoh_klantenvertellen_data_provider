<?php

namespace Drupal\klantenvertellen;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class KlantenvertellenFeedService.
 */
class KlantenvertellenFeedService
{

  protected $config;

  /**
   * Constructs a new KlantenvertellenFeedService object.
   */
  public function __construct(ConfigFactory $config)
  {
    $this->config = $config;
  }

  public function getData(): Klantenvertellen
  {
    return (new FeedParser(\Drupal::config('klantenvertellen.feedparameters')->get('feed_url')))->parse();
  }
}
