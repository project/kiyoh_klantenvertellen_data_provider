<?php

namespace Drupal\klantenvertellen;

abstract class Utils
{
  public static function getTempFileName($feedUrl): string
  {
    return 'klantenvertellen_cached_feed' . substr(base64_encode($feedUrl), -10, 10) . '.xml';
  }
}
