<?php

namespace Drupal\klantenvertellen;

class Klantenvertellen
{
  private $stars;

  private $averageScore;

  private $numberOfReviews;

  private $xmlObject;

  public function __construct(array $data)
  {
    foreach ($data as $prop => $value) {
      if (property_exists(static::class, $prop)) {
        $this->$prop = $value;
      }
    }
  }

  /**
   * Returns amount of stars on a scale from 0 to 5
   *
   * @return float
   */
  public function getStars(): float
  {
    return $this->stars;
  }

  public function getAverageScore(): float
  {
    return $this->averageScore;
  }

  public function getNumberOfReviews(): int
  {
    return $this->numberOfReviews;
  }

  public function getXmlObject():?\SimpleXMLElement
  {
      return $this->xmlObject;
  }

}
