<?php

namespace Drupal\klantenvertellen;

use SimpleXMLElement;
use Drupal\klantenvertellen\Exception\FeedNotCachedException;

class FeedParser
{
  private $feedUrl;

  public function __construct(string $feedUrl)
  {
    $this->feedUrl = $feedUrl;
  }

  public function parse(): ?Klantenvertellen
  {
    try {
      $xmlObject = $this->fetchXML();
      return new Klantenvertellen([
          'xmlObject' => $xmlObject,
          'stars' => round(($xmlObject->averageRating / 10) * 5),
          'averageScore' => (float) $xmlObject->averageRating,
          'numberOfReviews' => intval($xmlObject->numberReviews)
      ]);
    } catch (FeedNotCachedException $e) {
      throw $e;
    }
  }

  private function fetchXML(): ?SimpleXMLElement
  {
    $filePath = \Drupal::config('system.file')->get('path.temporary') . DIRECTORY_SEPARATOR . Utils::getTempFileName($this->feedUrl);
    if (empty($filePath) || !file_exists($filePath)) {
      throw new FeedNotCachedException;
    }
    libxml_use_internal_errors(true);
    if (!$xmlLoadResult = simplexml_load_file($filePath, null, LIBXML_NOCDATA)) {
      throw new FeedNotCachedException;
    }
    return $xmlLoadResult;
  }
}
