<?php

namespace Drupal\klantenvertellen\Plugin\QueueWorker;

/**
 * Imports XML feed contents to the cache
 *
 * @QueueWorker(
 *   id = "klantenvertellen_feed_importer",
 *   title = @Translation("Klantenvertellen queueworker, imports XML feed contents to the cache"),
 *   cron = {"time" = 60}
 * )
 */
class CronFeedImporterWorker extends FeedImporterWorker
{ }
