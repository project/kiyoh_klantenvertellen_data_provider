<?php

namespace Drupal\klantenvertellen\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\klantenvertellen\Exception\FeedNotFoundException;
use Drupal\klantenvertellen\Utils;

/**
 * Imports XML feed contents to the cache
 *
 */
abstract class FeedImporterWorker extends QueueWorkerBase
{
  /**
   * Process an importer item. Gets the contents op the feed and saves to temp-file
   *
   * @param StdClass $data object with one property 'feedUrl'
   * @return void
   * @throws \Exception
   */
  public function processItem($data)
  {
    try {
      if (!isset($data->feedUrl)) {
        throw new FeedNotFoundException('No feedUrl given to worker.');
      }
      $feedContent = $this->getFeedContent($data->feedUrl);
      $filePath = \Drupal::config('system.file')->get('path.temporary') . DIRECTORY_SEPARATOR . Utils::getTempFileName($data->feedUrl);
      file_put_contents($filePath, $feedContent);
    } catch (\Throwable $e) {
      $errorContent = 'Cannot save feed contents. ' . $e->getMessage();
      \Drupal::logger('klantenvertellen')->error($errorContent);
      throw new \Exception($errorContent);
    }
  }

  /**
   *
   * @param string $feedUrl
   * @return void
   * @throws FeedNotFoundException
   */
  private function getFeedContent(string $feedUrl)
  {
    $ctx = stream_context_create([
      'http' =>
      [
        'timeout' => 10,
      ]
    ]);
    $feedContent = \file_get_contents($feedUrl, false, $ctx);
    if ($feedContent === false) {
      throw new FeedNotFoundException;
    }
    return $feedContent;
  }
}
